package model.csv;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;

public class CSVFileReader implements Supplier<Map<String, String>> {
	private static final int FILE_NAME = 0;
	private static final int VIEW_NAME = 1;

	private String path;

	public CSVFileReader(String path) {
		this.path = path;
	}

	@Override
	public Map<String, String> get() {
		Map<String, String> nameMap = new LinkedHashMap<>();
		File file = new File(path);

		try (BufferedReader br = new BufferedReader(new FileReader(file));) {
			String line;
			while ((line = br.readLine()) != null) {
				String[] splittedStr = line.split(",");
				nameMap.put(splittedStr[FILE_NAME], splittedStr[VIEW_NAME]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return nameMap;
	}
}

package model.logic;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import model.csv.CSVFileReader;
import model.question.TFQuestion;
import model.xml.DOMXMLParser;
import model.xml.TFQuestionDataXMLParser;

public class FileLogic {

	public Map<String, String> readCSV(String path) {
		CSVFileReader csvReader = new CSVFileReader(path);

		return csvReader.get();
	}

	public List<TFQuestion> readXML(String path) {
		DOMXMLParser<TFQuestion, List<TFQuestion>> parser;
		List<TFQuestion> questionList = null;

		try {
			parser = new TFQuestionDataXMLParser(path);
			questionList = parser.parse();
		} catch (ParserConfigurationException | SAXException | IOException e) {
			e.printStackTrace();
		}

		return questionList;
	}

	public boolean generateXML(String path) {
		return false;
	}
}

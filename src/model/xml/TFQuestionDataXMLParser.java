package model.xml;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import model.question.TFQuestion;

public class TFQuestionDataXMLParser
extends DOMXMLParser<TFQuestion, List<TFQuestion>> {

	public TFQuestionDataXMLParser(String path)
			throws ParserConfigurationException, SAXException, IOException {
		super(path);
	}

	/**
	 *  xmlを解析する。
	 */
	public List<TFQuestion> parse() {
		List<TFQuestion> questionList = new ArrayList<TFQuestion>();

		System.out.println("Parse Start");
		try {
			Document doc = builder.parse(this.file);
			Node listNode = doc.getDocumentElement();
			Node elementNode = listNode.getFirstChild();

			while (elementNode != null) {
				if (isTextNode(elementNode)) {
					elementNode = elementNode.getNextSibling();
					continue;
				}

				if (elementNode.getNodeName().equals("Question")) {
					questionList.add(parseQuestionNode(elementNode));
				}
				elementNode = elementNode.getNextSibling();
			}
		} catch (SAXException | IOException e) {
			e.printStackTrace();
			System.out.println("Parse Error!");
		} finally {
			System.out.println("Parse End");
		}

		return questionList;
	}

	private TFQuestion parseQuestionNode(Node questionNode) {
		String sentence = null;
		String explain = null;
		Boolean answer = null;
		String tChoose = null;
		String fChoose = null;

		Node elementNode = questionNode.getFirstChild();
		while (elementNode != null) {
			if (isTextNode(elementNode)) {
				elementNode = elementNode.getNextSibling();
				continue;
			}

			Node textNode;
			String value;
			if ((textNode = elementNode.getFirstChild()) == null ||
					(value = textNode.getNodeValue()) == null ||
					value.equals("")) {
				elementNode = elementNode.getNextSibling();
				continue;
			}

			String name = elementNode.getNodeName();
			switch (name) {
			case "Sentence":
				sentence = value;
				break;
			case "Explain":
				explain = value;
				break;
			case "Answer":
				answer = Boolean.parseBoolean(value);
				break;
			case "TChoose":
				tChoose = value;
				break;
			case "FChoose":
				fChoose = value;
			default:
				break;
			}
			elementNode = elementNode.getNextSibling();
		}

		return new TFQuestion(sentence, explain, answer, tChoose, fChoose);
	}

}

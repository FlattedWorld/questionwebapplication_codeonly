package model.xml;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

/**
 * DOM形式でのXML解析、編集メソッドを提供する抽象クラス
 *
 * @author FW
 *
 * @param <T> コレクション対象のインスタンスの型
 * @param <U> コレクションインスタンスの型（List, Set, Map）
 */
public abstract class DOMXMLParser<T, U extends Collection<T>> {
	private DocumentBuilderFactory factory;
	protected DocumentBuilder builder;
	protected File file;

	/**
	 * ファイルパスの指定がない、コンストラクタ
	 * @throws ParserConfigurationException
	 */
	public DOMXMLParser() throws ParserConfigurationException {
		this.factory =
				DocumentBuilderFactory.newInstance();
		this.builder =
				this.factory.newDocumentBuilder();
	}


	/**
	 * ファイルパスの指定がある、コンストラクタ
	 * @param path ファイルパス
	 * @throws SAXException
	 * @throws IOException
	 * @throws ParserConfigurationException
	 */
	public DOMXMLParser(String path)
			throws SAXException,
			IOException,
			ParserConfigurationException {
		this();
		this.file = new File(path);
	}

	/**
	 * 解析を行う抽象メソッド
	 * @
	 * @return
	 */
	public abstract U parse();

	/**
	 * ファイル指定を行うメソッド
	 * @param path ファイル位置
	 */
	public void setFile(String path) {
		this.file = new File(path);
	}

	/**
	 *  テキストノードか判別する
	 * @param node 判別対象ノード
	 * @return trueなら、テキストノードである
	 */
	protected boolean isTextNode(Node node) {
		if (node != null && node.getNodeType() == Node.TEXT_NODE) {
			return true;
		}
		return false;
	}

	/**
	 * ノードから、属性を取得する。
	 * @param node ノード
	 * @param name 属性名
	 * @return 属性が格納していたデータ
	 */
	protected String getAttribute(Node node, String name) {
		NamedNodeMap attributes = node.getAttributes();
		Node attribute = attributes.getNamedItem(name);

		return attribute.getNodeValue();
	}

}

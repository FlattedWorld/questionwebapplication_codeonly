package model.question;

import java.io.Serializable;

/**
 * 問題データ用JavaBeans抽象クラス
 *
 * @author FW
 */
public abstract class Question implements Serializable {
	private String sentence; // 問題文
	private String explain;

	public Question() {

	}

	public Question(String sentence, String explain) {
		this.sentence = sentence;
		this.explain = explain;
	}

	public String getSentence() {
		return sentence;
	}

	public String getExplain() {
		return explain;
	}

}

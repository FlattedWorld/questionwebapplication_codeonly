package model.question;

import java.io.Serializable;
import java.util.HashMap;

/**
 * 2択問題用クラス、真偽値で回答を行う。
 *
 * @param sentence 問題文
 * @param explain 説明文
 * @param answer 正答
 * @param TChoose 真となる選択文
 * @param FChoose 偽となる選択文
 */
public class TFQuestion extends Question implements Serializable {
	private boolean answer;
	private HashMap<Boolean, String> choice = new HashMap<>();

	public TFQuestion() {
	}

	public TFQuestion(String sentence, String explain,
			boolean answer, String TChoose, String FChoose) {
		super(sentence, explain);
		this.answer = answer;
		this.choice.put(true, TChoose);
		this.choice.put(false, FChoose);
	}

	public boolean isAnswer() {
		return answer;
	}

	public HashMap<Boolean, String> getChoice() {
		return choice;
	}
}

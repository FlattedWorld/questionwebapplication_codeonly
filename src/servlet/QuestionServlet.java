package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.logic.FileLogic;
import model.question.TFQuestion;

/**
 * Servlet implementation class QuestionServlet
 */
@WebServlet("/QuestionServlet")
public class QuestionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private Map<String, String> nameMap;
	private Map<String, List<TFQuestion>> questionMap = new HashMap<>();;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public QuestionServlet() {
		super();
	}

	/**
	 *  スコープ別に格納されるインスタンスと名称</br>
	 * Application</br>
	 * "nameMap":Map String&String 別名を格納したマップ</br>
	 * "questionMap":Map String&List TFQuestion 問題リストを格納したマップ</br>
	 *
	 * @see HttpServlet#init()
	 */
	synchronized public void init(ServletConfig config) throws ServletException {
		ServletContext application = config.getServletContext();
		FileLogic logic = new FileLogic();

		String csvPath =
				application.getRealPath("/WEB-INF/CSV/Name.csv");
		nameMap = logic.readCSV(csvPath);

		questionMap = new HashMap<>();
		for (String fileName : nameMap.keySet()) {
			StringBuilder filePath = new StringBuilder("/WEB-INF/XML/")
					.append(fileName)
					.append(".xml");
			String xmlPath =
					application.getRealPath(filePath.toString());
			questionMap.put(fileName, logic.readXML(xmlPath));
		}
		application.setAttribute("nameMap", nameMap);
		application.setAttribute("questionMap", questionMap);
	}

	/**
	 * スコープ別に格納されるインスタンスと名称</br>
	 * Session</br>
	 * ・"questionList":List TFQuestion 解析した問題データのリスト</br>
	 * ・"userQuestionList":List TFQuestion ユーザー依存の問題リスト</br>
	 * ・"listName":String 問題リストの名前
	 * ・"correctCount":Integer 正答数（開始なら0からスタート</br>
	 * ・"number":Integer 出題数
	 * Request</br>
	 * ・"correctCount":Integer 問題が完了したら、スコープを移動する。</br>
	 *
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		boolean hasError = false;

		// グローバル環境のリスト取得
		// href="/QuestionServlet?path=[File name]" pathはXMLを読み出す。
		List<TFQuestion> questionList =
				(List<TFQuestion>)session.getAttribute("questionList");
		String fileName = request.getParameter("path");
		if (questionList == null || fileName != null) {
			if (fileName == null || (!questionMap.containsKey(fileName))) {
				// 指定がない、もしくは誤っている場合はエラー
				hasError = true;
			} else {
				resetAttribute(session);
				questionList = questionMap.get(fileName);
				String name = nameMap.get(fileName);
				session.setAttribute("questionList", questionList);
				session.setAttribute("listName", name);
			}
		}

		if (hasError) {
			response.sendRedirect("/QuestionServletSite/JSP/ErrorPage.jsp");
		} else {
			// ユーザー環境のリスト取得
			StringBuilder path = new StringBuilder("/WEB-INF/JSP/");
			List<TFQuestion> userQuestionList =
					(List<TFQuestion>)session.getAttribute("userQuestionList");
			if (userQuestionList == null) {
				userQuestionList = new ArrayList<TFQuestion>(questionList);
				if (request.getParameter("isRandom") != null) {
					shaffleList(userQuestionList);
				}
				session.setAttribute("userQuestionList", userQuestionList);
				session.setAttribute("correctCount", 0);
			}

			// href="/QuestionServlet?isEnd=true" isEndはパラメーター名
			boolean isEnd = request.getParameter("isEnd") != null;
			if (userQuestionList.size() == 0 || isEnd) {
				// 問題がない->リザルト行き

				int correctCount =
						(Integer)session.getAttribute("correctCount");
				int number = (Integer)session.getAttribute("number");
				if (isEnd) {
					number--;
				}
				resetAttribute(session);
				request.setAttribute("number", number);
				request.setAttribute("correctCount", correctCount);

				path.append("Result.jsp");
			} else {
				// 問題がある->問題行き

				TFQuestion question = userQuestionList.remove(0);
				int number = questionList.size() - userQuestionList.size();
				session.setAttribute("number", number);
				session.setAttribute("userQuestionList", userQuestionList);
				session.setAttribute("question", question);

				path.append("Question.jsp");

			}
			RequestDispatcher dispatcher = request.getRequestDispatcher(path.toString());
			dispatcher.forward(request, response);
		}
	}

	/**
	 * スコープ別に格納されるインスタンスと名称</br>
	 * Session</br>
	 * ・"correctCount":Integer 正解だったら更新</br>
	 * Request</br>
	 * ・"iscorrect":Boolean 正解かどうか</br>
	 * ・"question":TFQuestion スコープ移動した問題データ</br>
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();

		TFQuestion question =
				(TFQuestion)session.getAttribute("question");
		boolean answer =
				Boolean.parseBoolean(request.getParameter("select"));
		boolean isCorrect = answer == question.isAnswer();
		if (isCorrect) {
			int correctCount =
					(Integer)session.getAttribute("correctCount");
			session.setAttribute("correctCount", ++correctCount);
		}
		request.setAttribute("isCorrect", isCorrect);
		request.setAttribute("question", question);

		// 解説行き
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/JSP/Explain.jsp");
		dispatcher.forward(request, response);
	}

	private void resetAttribute(HttpSession session) {
		session.removeAttribute("questionList");
		session.removeAttribute("userQuestionList");
		session.removeAttribute("listName");
		session.removeAttribute("question");
		session.removeAttribute("correctCount");
		session.removeAttribute("number");
	}

	private void shaffleList(List<TFQuestion> questionList) {
		int size = questionList.size();
		java.util.Random rand = new java.util.Random();

		for (int index = 0; index < size; index++) {
			TFQuestion source = questionList.get(index);
			int targetIndex = rand.nextInt(size);
			TFQuestion target = questionList.set(targetIndex, source);
			questionList.set(index, target);
		}
	}
}

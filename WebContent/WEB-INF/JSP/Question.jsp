<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- 使用するもの questionList(Session), questionList(Application) -->
<!--			  question(Session) -->

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>${listName}</title>
		<link rel="stylesheet" href="/QuestionServletSite/css/cjava.css">
		<script src="/QuestionServletSite/JS/QuestionControll.js"></script>
	</head>
	<body>

		<h1>${listName}</h1>
		<h2>第${number}問</h2>
		<p>${question.sentence}</p>
		<form action="/QuestionServletSite/QuestionServlet" method="post">
		<div id="selectBox">
			<h3>解答</h3>
			<br>
			<div>
				<span>
					<c:choose>
						<c:when test="${not empty question.choice[true]}">
							<input onclick="checked()" type="radio" name="select" value="true" checked>${question.choice[true]}
						</c:when>
						<c:otherwise>
							<input onclick="checked()" type="radio" name="select" value="true" checked>正しい。
						</c:otherwise>
					</c:choose>
				</span>
				<span>
					<c:choose>
						<c:when test="${not empty question.choice[false]}">
							<input onclick="checked()" type="radio" name="select" value="false">${question.choice[false]}
						</c:when>
						<c:otherwise>
							<input onclick="checked()" type="radio" name="select" value="false">誤りである。
						</c:otherwise>
					</c:choose>
				</span>
				<br>
				<input id="submit" type="submit"value="確定">
				<button type="button" onclick="check('skip')">
					問題をスキップ
				</button>
				<button type="button" onclick="check('end')">
					問題を中断
				</button>
				</div>
			</div>
		</form>
	</body>
</html>
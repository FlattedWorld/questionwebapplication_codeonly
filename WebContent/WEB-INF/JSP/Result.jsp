<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>結果発表!</title>
		<link rel="stylesheet" href="css/kekka.css">
	</head>
	<body>
	<div id="ds">
		<h1>結果発表～!!!</h1><br>

		<c:choose>
			<c:when test="${number != 0}">
				あなたは${number}問中${correctCount}問、正解しました。<br>
				正答率:${(correctCount / number)*100}%<br>

			</c:when>
			<c:otherwise>
				<script>
					location.href="/QuestionServletSite";
				</script>
			</c:otherwise>

		</c:choose>
     <c:choose>
         <c:when test="${(correctCount/number)*100>=60 }">
          たいへんよくできました!<br>
          </c:when>
          <c:otherwise>
          残念!!(´・ω・｀) もう一度、最初から挑戦しよう!<br>
          </c:otherwise>
     </c:choose>
     <c:if test="${(correctCount/number)*100<=60}">
    <h1>不合格ですね。</h1>
     <img alt="がんばってください。" src="/QuestionServletSite/images/fuseikai3.png">
    <div class="fugoukaku"></div>
     </c:if>

		<button onclick="location.href='/QuestionServletSite'">トップに戻る</button>

    </div>

	</body>
</html>


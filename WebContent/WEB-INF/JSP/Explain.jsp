<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Explain</title>
		<link rel="stylesheet" href="css/cjava.css">
	</head>
	<body>
		<h1>${listName}</h1>
		<br>
		<h1>第${number}問</h1>

		<c:choose>
			<c:when test="${isCorrect}">
				<div class="seikai">
					<h2>正解</h2>
				</div>
				<br>
			</c:when>
			<c:otherwise>
				<div class="fuseikai">不正解</div>
				<img src="">
				<br>
				<script src="/QuestionServletSite/JS/ChangeImage.js">
				</script>
			</c:otherwise>
		</c:choose>

		<h2>問題文</h2>
		<p>${question.sentence}</p>
		<br>
		<h2>解説</h2>
		<p>
			<c:choose>
				<c:when test="${empty question.explain}">
					解説はありません。
				</c:when>
				<c:otherwise>
					${question.explain}
				</c:otherwise>
			</c:choose>
		</p>
		<br>
		<button type="button" onclick="location.href='/QuestionServletSite/QuestionServlet'">
			次の問題へ
		</button>
	</body>
</html>
"use strict";

// Eclipseではエラーが出るが、動的インポートの手法でJQuery宣言する。
var $ = import("/QuestionServletSite/JS/jquery-3.5.1.js");

/**
 * エレメント要素からクエリ要素に変換する。（備忘録）
 *
 * @param element エレメント
 * @returns エレメントを変換したクエリ
 */
function elementToJquery(element) {
	return $(element);
}

/**
 * クエリ要素からエレメント要素に変換する。（備忘録）
 *
 * @param jquery クエリ
 * @returns クエリを変換したエレメント
 */
function jqueryToElement(jquery) {
	return jquery[0];
}

var showDownList = function(no) {
	let question = document.getElementsByClassName("question")[no];
	question = elementToJquery(question);

	if (question.is(":hidden")) {
		question.slideDown("fast");
	} else {
		question.slideUp();
	}
}

var addList = function() {
	let count = document.getElementsByTagName('ul').length;
	let input =
		'<ul class=\"question\">' +
		'<li>問題文　<textarea name=\"sentence' + (count + 1) + '\"' +
		' rows=\"5\" cols=\"100\" wrap=\"soft\" required></textarea></li>' +
		'<li>解説　　<textarea name=\"explain' + count + '\"' +
		' rows=\"5\"cols=\"100\" wrap=\"soft\" required></textarea></li>' +
		'<li>正解とする選択肢<br>' +
		'<input type=\"radio\" value=\"true\" name=\"answer' + count + '\" checked>選択肢１ ' +
		'<input type=\"radio\" value=\"false\" name=\"answer' + count + '\">選択肢２' +
		'</li>' +
		'<li>選択肢１の文章　' +
		'<input type=\"text\" name=\"tChoose' + count + '\"></li>' +
		'<li>選択肢２の文章　' +
		'<input type=\"text\" name=\"fChoose' + count + '\"></li>' +
		'<li><button class=\"removeButton\" type=\"button\" onclick=\"removeList(' + count +')\">削除</button></li>' +
		'</ul>';

	$(function() {
		$("#list").append('<div onclick=\"showDownList(' + count + ')\">問題' + (count + 1) + '</div>')
				.append(input)
	});
}

var removeList = function(no) {
	const inputNames = ['sentence', 'explain', 'answer', 'answer', 'tChoose', 'fChoose'];

	let questionElements = document.getElementsByClassName("question");
	let headerElements = document.getElementById("list").getElementsByTagName("div");

	$(questionElements[no]).remove();
	$(headerElements[no]).remove();

	questionElements = document.getElementsByClassName("question");
	headerElements = document.getElementById("list").getElementsByTagName("div");

	for (let index = 0; index < questionElements.length; index++) {
		let inputs = questionElements[index].querySelectorAll("textarea, input");

		for (let inputIndex = 0; inputIndex < inputs.length; inputIndex++) {
			inputs[inputIndex].setAttribute('name', inputNames + index);
		}

		console.log(questionElements[index] == null)
		questionElements[index].querySelectorAll('button')[0]
				.setAttribute("onclick", "removeList(" + index + ")");

		headerElements[index].setAttribute("onclick", "showDownList(" + index + ")");
		headerElements[index].innerHTML = "問題" + (index + 1);
	}

}

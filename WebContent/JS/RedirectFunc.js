/**
 * ボタンリダイレクト用関数
 */

"use strict"

var redirectToServlet = function(fileName) {
	let url =
		"/QuestionServletSite/QuestionServlet?path=" + fileName;
	let isRandom = document.getElementById("Random");

	if (Random.checked) {
		url += "&isRandom=true"
	}

	window.location.href = url;
}


"use strict";

var check = function checkAccept(type) {
	let message;
	let url = "/QuestionServletSite/QuestionServlet";

	switch (type) {
	case "skip":
		message = "スキップしますか？";
		break;
	case "end":
		message = "中断しますか？";
		url += "?isEnd=true";
		break;
	default:
		break;
	}

	if (confirm(message)) {
		window.location.href = url;
	}
}
